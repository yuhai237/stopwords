# 中文常用停用词表

| 词表名 | 词表文件 |
| - | - |
| 中文停用词表                   | cn\_stopwords.txt    |
| 哈工大停用词表                 | hit\_stopwords.txt   |
| 百度停用词表                   | baidu\_stopwords.txt |
| 四川大学机器智能实验室停用词库 | scu\_stopwords.txt   |
| CSDN作者(阿瞒有我良计15)分享的停用词库 | csdn\_stopwords.txt   |
| 去重后的停用词库 | deduplication\_stopwords.txt   |



